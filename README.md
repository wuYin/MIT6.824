# MIT 6.824: Distributed Systems

## 课程概览

2018 Spring 课程：[pdos.csail.mit.edu/6.824/schedule.html](https://pdos.csail.mit.edu/6.824/schedule.html)

- [x] Lab1：MapReduce

- [x] Lab2：Raft
  - [x] 2A：Leader Election
  - [x] 2B：Log Replication
  - [x] 2C：State Persistence and Unreliable Network
- [x] Lab3：Fault-tolerant Key/Value Service
  - [x] 3A：KV service without log compaction
  - [x] 3B：KV service with log compaction
- [ ] Lab 4: Sharded Key/Value Service

## 课程目标

从 MapReduce 计算模型入门，到实现 Raft 一致性算法，并进一步构建容错的分布式 kvDB，并构建 kvDB 集群。

## 课程结构

6.824 是 MIT 开设的分布式系统课程，课程共计 22 节，每堂课都有 lecture 讲义、Paper 阅读及 FAQ 答疑。

**课程有 4 个 Lab 实验：**

- Lab1 MapReduce：熟悉分布式基础概念

  阅读论文并梳理 MR 模型的执行流程，实现单机版、分布式版的 word count，最后使用模型来生成倒排索引。

- Lab2 Raft：分三个模块实现 Raft 一致性算法

  - 2A Leader Election：实现 leader 选举和心跳通信，处理好网络分区、多节点失效及 split vote
  - 2B Log Replication：实现日志复制和一致性检查，在少部分节点失效时依旧能 commit 日志，并解决好日志冲突。
  - 2C Network Unavailable：实现节点状态的持久化和重启读取，应对 RPC 请求乱序、延迟甚至丢失的网络环境、节点频繁崩溃和重启的情况。

- Lab3 kvDB：基于 Raft 实现线性一致的分布式容错 kv 数据库

  - 3A：基于 Raft 库保证在并发请求下数据的线性一致性，处理 RPC 超时重试，实现请求去重等逻辑。
  - 3B：实现 Raft 的日志压缩及数据快照，实时监测日志大小并剪切，切换 RPC 加速日志回放。

- Lab4 Sharded kv：实现 Raft 的 Membership Change 配置更新，构建 kvDB 集群。实现中 …

每个 Lab 的环境代码都有注释提示，都有写好的单元测试，目标是每个测试跑 `go test -race -count` 多次都能 pass

## 课程难点

- 论文阅读

  MR、Raft 等 lab 相关的论文要反复阅读，尤其是 Raft 论文的图 2，一定要把每个细节理解透彻。

- 调试代码

  坑真的非常多。所以写代码前认真阅读 Lecture 并思考，参考其中的 Hint 提示，阅读课程对 Raft 结构的解析。此外，课程助教写的 [Students' Guide to Raft](https://thesquareplanet.com/blog/students-guide-to-raft/) 值得参考。

## 课程笔记

目前我只完成前三个 Lab，工作之余耗时近三个月。实验遇到的坑记录在了我的博客，希望有所帮助：

- [Lab1: MapReduce 论文和实验笔记](https://github.com/wuYin/blog/blob/master/distributed_systems/map-reduce.md)
- [Lab2A: Raft 选主实现](https://github.com/wuYin/blog/blob/master/distributed_systems/raft-lab2A-election.md)
- [Lab2B: Raft 日志复制实现](https://github.com/wuYin/blog/blob/master/distributed_systems/raft-lab2B-log-replication.md)
- [Lab2C: Raft 数据持久化与不可靠网络处理](https://github.com/wuYin/blog/blob/master/distributed_systems/raft-lab2C-persistence-and-network-unavailable.md)
- [Lab3A: 基于 Raft 实现容错的 kvDB](https://github.com/wuYin/blog/blob/master/distributed_systems/raft-lab3A-fault-tolerant-kv-service.md)
- [Lab3B: Raft 日志压缩及数据快照](https://github.com/wuYin/blog/blob/master/distributed_systems/raft-lab3B-kv-service-log-compaction.md)
- [Raft 个人笔记](https://github.com/wuYin/blog/blob/master/distributed_systems/raft-notes.md)