---
title: Fault Tolerance. Raft(2)
date: 2019-05-15 09:20:01
tags: 分布式系统
---

原 Lecture：[l-raft2.txt](https://pdos.csail.mit.edu/6.824/notes/l-raft2.txt)

<!-- more -->

> Big Picture：完成 Lab3 中的 key/value 存储服务。目标：
>

- 在客户端看来，节点集群是单一的  non-replicated 节点
- 少数节点宕机，整个集群依旧可用

```
client RPC -> Start() -> majority commit protocol -> applyCh // 请求处理流程
```



> 为什么要用大多数？

因为两个大多数 commit majority 和 vote majority 必然会有 overlap，从而保证后续选出来的 leader 包含先前所有 committed 的日志。



### Topic：Raft Log 2B

> 我们要确保实现什么？

如果一个节点在本地执行了一条命令，那其他节点在此处不可能会执行其他目录，即是状态机安全原则。如下，不能出现 S1 和 S2 在索引为 2 处执行不同目录。

```
S1: put(k1,v1) | put(k1,v2) | ...
S2: put(k1,v1) | put(k2,x)  | ...
```



> 如何解决日志不一致问题？（对实现比较重要的点）

leader 强制同步自己的日志给 follower 以达成共识，假设现在集群日志如下：

```
S1:  3
S2:  3  3  4
S3:  3  3  5
```

- S3 在 term6 被选为新 leader

- S3 向其他两个节点发送 AppendEntries RPC，entry 为 13，prevLogIndex=12，prevLogTerm=5
- S2 的调用会返回 false
- S3 将 nextIndex[S2] 递减为 12
- S3 将 12+13 entries 的日志一并发送给 S2，prevLogIndex=11，prevLogTerm=3
- S2 一致性检查成功，**删除**自己的 12 号冲突日志。达成共识



> 如何确保 S2 删除 12th 冲突日志是安全的？换句话说，如何保证 leader 不会回滚日志，即丢弃旧 leader 已提交的日志？
>

Raft 能通过限制选举保证选出来的 leader 拥有所有 committed 日志。选举限制 candidate 的日志必须至少要 up-to-date：

- candidate 的最后一条日志 term 更大
- candidate 最后一条日志 term 一样，日志一样长，或拥有更长的日志



> 如何快速地回滚 follower 的冲突日志？
>

```
S1: 4 5 5      4 4 4      4
S2: 4 6 6  or  4 6 6  or  4 6 6
S3: 4 6 6      4 6 6      4 6 6
```

S3 是 term6 的 leader，S1 现在重新上线。如果 follower 拒绝 AppendEntries RPC，那在 reply 中带上：follower 在冲突索引处的任期号 & follower 的该任期首条日志的索引

- 若 leader 本地有 follower 冲突 term 的日志：将 nextIndex[i] 挪到该 term 的最后一条处。

  说明 follower 的冲突不算严重，只是本地还有旧 leader 的 uncommitted 日志，此时 leader 要做的是把下一个 term 的日志发送同步即可。**如下 d 将 5 5 发送给 e 即可：**

  ![image-20190515154729366](https://images.yinzige.com/2019-05-15-074729.png)

- 否则，nextIndex[i] 直接回滚到 conflict index 处。

  follower 的冲突最为严重，即日志是完全 uncommitted 的，强制覆盖整个 term 的日志。**如上图中的 S1 为 leader 时的 5 5 两条日志。**



### Topic：状态持久化 2C

> Raft 要能处理好 crash 后 reboot 的情况，两种很有用的策略：
>

- 用全新的节点替换 crash 的节点：需要同步大量日志或快照，比较慢
- 重启 crash 的节点，re-join 集群：必须实现状态的持久化，以便于日志的 catch up，以应对断电等情况



> 持久化可能会是性能瓶颈

硬盘读写速度 10ms 级别，SSD 读写在 0.1ms，所以持久化写数据的 OPS 在 100-10000 间。

解决方案：

- 批量写入日志
- 不把日志存放在磁盘，放在 battery-backed 的内存中





### Topic：log 的压缩和快照 3B

问题：日志很多之后，重启或新节点加入，回放日志很慢

> 节点丢弃日志有哪些限制？

- 不能删 uncommitted 的日志：可能 leader 正在统计 commit
- 不能删 unexected 的日志



































