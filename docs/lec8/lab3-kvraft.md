## Lab 3: Fault-tolerant Key/Value Service

### 简介

目标：使用 Lab2 的 Raft 库构建一个容错的 key/value 存储服务。服务只需支持 3 个操作：

```go
Put(key, value)  // 更新指定的键值
Append(key, arg) // append 指定的键值，若 key 不存在则 Put
Get(key)         // 获取键值
```

client 与 Clerk 交互，Clerk 通过实现  Get/Put/Append RPC 与 Raft 节点交互。

在实现  Get/Put/Append 时必须保证分布式操作的线性一致性：

- 若是顺序调用：那对 client 看起来像是只在操作一份数据，每次操作产生的影响对后续操作都可见。
- 若是并发调用：返回结果和最终状态必须是一致的。

Lab 分割：

- PartA：实现 kv 服务
- PartB：实现 snapshot 快照功能，垃圾回收旧日志



### PartA: Key/value service without log compaction

每台 kv server 都是一个 Raft 节点，Clerks 会向 leader 发起 Get/Put/Append 请求，Raft log 会顺序保存操作命令，之后在本地的 kv 数据库中顺序执行。总体目标：让多个 kv 节点的数据库保存相同的副本。

Clerk 不知哪个节点是 leader 或无法 reach 节点，此时应不断更换节点重试。kv 服务尝试将命令日志 commit 并将结果返回给 Clerk。若 commit 失败（如 leader 变更），则 Clerk 应重试。

#### Task

第一个 task 是在正常网络、无失效节点的集群中实现 kv 服务。

- 需在 `client.go` 中实现 Get/Put/Append 操作中实现 RPC-sending 逻辑。

- 需在 `server.go` 的 RPC handlers 中实现 Get/Put/Append 逻辑，handlers 应调用 Raft 的 Start() 来发送 Op

#### Note

kv 节点间不应该直接通信，彼此间应只通过 Raft log 进行交互

#### Hint

- 节点调用 `Start()` 后需等待 Raft 达成共识，注意读取 applyCh 的方式，别造成死锁
- 处理好 case：Clerk 请求 leader 执行命令，但 leader 还没来得及将该日志 commit 就丢失了 leader 地位，此情况 Clerk 应向其他节点重试请求直到发现了新 leader，实现：
  - for the server to detect that it has lost leadership, by noticing that a **different request has appeared** at the index **returned by Start()**：Start() 已返回的序号上居然出现了不同的请求
  - or that Raft's term has changed

- 你可能要修改 Clerk 来保存处理最新 RPC 时的 leader 位置，下次 RPC 直接请求该 laeder
- kv 节点如果不再是大多数节点之一，那就不应处理 Get 请求返回旧数据。简单实现：每次操作不必实现论文第八节描述的为只读操作所作出的优化
- 不要写竞态代码：test -race





因为 Clerk 有 re-send 的操作，所以 kv 服务要有机制来保证不会重复处理统一命令。

#### Task

复制一份 Clerk 的 request，实现每次请求只发送一次。通过 3A 测试

#### Hint

- 需要唯一标识每次请求，以确保 kv 服务不会重复处理命令。
- 对于重复命令的检测应快速



> 3A 实验笔记

#### Server 端

**Get 操作：**

参考论文中的第八节，在 Raft 节点在处理 GET 请求时，必须先通过一次同步心跳来确保自己的领导地位。即：

- 若节点不是 leader 直接告知 client 换个节点重试
- 对于 leader 节点，可能之前的 SET 请求还未 commit 完毕，所以要保证本次 GET 能获取最新的数据，就要等 commit 结束
- 手动发起一次 **同步** 心跳，统计一致性检查成功的副本节点数量，若达大多数则说明 leader 的数据是最新的



**Put 操作**

- 切换直到找到 leader，调用 Start() 开启 agreement，记录好 index
- 等待 leader 的 applyCh 中传来该 index 日志已 apply 的消息





























