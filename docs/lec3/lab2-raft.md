---
title: Lab2. Raft
date: 2019-04-16 08:48:24
tags: 分布式系统
---

Lab2 Raft 实验笔记。

<!-- more -->

先熟悉论文，看 lab 讲义，切入实验部分。

## Part 2A

### 目标

实现 leader 选举和心跳请求（不含日志的 AppendEntries RPC 请求），本小节目标：

- 选出单一的 leader，一直保持领导身份直到自己出错崩溃
- 若前任 leader 崩溃，或前任 leader 通信的包丢失，新选出的 leader 要能正确处理

使用 `go test -run 2A` 来测试本节代码。



### 提示

- 向 raft.go 的 Raft 结构添加字段来完善 Raft 节点，需要定义要存储的日志条目信息。实现尽可能与论文图 2 保持一致。

- 完善 `RequestVoteArgs` 和 `RequestVoteReply` 结构，修改 `Make()` 创建后台 goroutine，若它等待一个选举超时时间后还未接收到来自其他节点的请求，就发起 `RequestVote` RPC 调用。如果此时集群中已有 leader 或它被选为了新 leader，其他节点都能知道。实现 `RequestVote()` RPC 来让节点能进行投票。

- 为实现心跳请求，定义一个 AppendEntries RPC 结构（定义全一点，哪怕有的字段你现在用不着），同时实现 AppendEntries RPC handler 方法来重置其余节点的选举超时计数器，以防止在已有 leader 的情况下还选出了新 leader

- 确保每个 follower 不会同时选举超时，否则它们只会各自给自己投一票，导致选票被瓜分无法形成大多数，也就产生不了 leader

- tester 要求 leader 每秒发送的心跳请求不超过 10 个

- 若 leader 崩溃，集群内的大多数节点依旧能相互通信，那 tester 要求在奔溃后的 5s 内选举出新 leader。注意，选举如果因为可能要进行好几轮才能选出 leader（发生丢包、多个候选人不小心用了一样的选举超时时间）。你必须找一个足够短的选举超时时间（和心跳间隔时间）来让每次选举都保证在 5s 内完成

- 论文 5.2 节提到选举超时时间在 150-300ms 范围内随机选取，只有 leader 在 150ms 内会发送多个心跳请求，此范围才是合理的。由于 tester 限制了每秒最多发送 10 次心跳请求，所以你的选举超时时间肯定要比 150-300ms 要大，但是也别大太多，因为系统只能忍耐 5s 内选出 leader

- rand 可生产随机数

- 对于周期性操作，最简单的实现就是在一个 goroutine 的死循环中调用 time.Sleep()，也可以用 Go 语言中 time 包中的  Timer 和 Ticker 来实现

- 实验中锁的使用可参考：[lock advice](https://pdos.csail.mit.edu/6.824/labs/raft-locking.txt)

- 如果你的实现无法通过测试，那就重读论文中的图2，有关 leader 选举的所有逻辑都在里边

- 调试代码的好办法就是把参数打印出来并追踪，如果输出太长可重定向到文件中慢慢排查，util.go 中的 `DPrintf` 可能对调试有用

- Go RPC 发送的参数结构必须是大写开头可导出的，内部的子结构也一样要大写，否则 labgob 会给出 warning，不要忽略 warning

- 使用 `go test -race` 来检测并解决代码中隐藏的竞态条件



## Part 2B

### 目标

通过复制操作日志的方式来保证节点的数据一致性。实现 leader 的 `Start()` 用于处理新日志，同时 leader 对其他节点发起 `AppendEntries` RPC 调用。

实现 `Start()` 方法，完善 AppendEntries RPC 的结构并处理请求，会用到 leader 的 `commitIndex`

### 提示

- 本节要实现论文中 5.4 节对 leader 选举的限制

- Lab2B 测试不通过可能是因为集群中存在正在进行 un-needed 选举的节点，即使集群已有可和大多数节点通信的 leader，这将导致测试用例认为集群不可能达成数据一致。

  选举超时计时器管理不当、赢得选举后 leader 未能及时发送心跳请求等都可能造成集群发生 un-needed 选举。

- 本节你需要实现事件驱动的同步阻塞机制，对于需同步执行的代码不要在循环中无节制地一直执行，否则可能测试不通过。可使用 Golang 的 channel、条件变量等机制实现同步阻塞，再不行 `time.Sleep(10 * time.Millisecond)` 也行。
- 尽可能地把自己本节的并发代码写得干净整洁，结构组织得精炼写，因为后边的 lab 还要回头改本节的代码。

如果你的实现执行速度不快，可能无法通过 lab2B 测试。标准的测试输出：

```
$ time go test -run 2B
Test (2B): basic agreement ...
  ... Passed --   0.5  5   28    3
Test (2B): agreement despite follower disconnection ...
  ... Passed --   3.9  3   69    7
Test (2B): no agreement if too many followers disconnect ...
  ... Passed --   3.5  5  144    4
Test (2B): concurrent Start()s ...
  ... Passed --   0.7  3   12    6
Test (2B): rejoin of partitioned leader ...
  ... Passed --   4.3  3  106    4
Test (2B): leader backs up quickly over incorrect follower logs ...
  ... Passed --  23.0  5 1302  102
Test (2B): RPC counts aren't too high ...
  ... Passed --   2.2  3   30   12
PASS
ok      raft    38.029s

real    0m38.511s
user    0m1.460s
sys     0m0.901s
```

ok 38.029s 是指整个 lab2B 测试耗时。user 1.460s 则是用户真正占用 CPU 的时间，即 CPU 真正花在计算上的时间。如果你测试时间超过了 1 分钟，或 user CPU 时间超过了 5s，随后的实验可能会因为代码低效产生问题，遇到这种问题可去找 sleep 代码、RPC 可能调用超时的地方，以及等待 channel 等可能产生阻塞的地方。





## Part 2C

实时的持久化操作：每次更新日志都写入磁盘，每次读取数据都读最新的一份。为求简单，本 lab 中将不使用磁盘存储，而使用 `Persister` 对象来模拟。

TASK：完善 persist() 和 readPersist() 用于存储和恢复 Raft 的节点状态，参考注释使用 labgob 编码。

TASK：你在 Raft 实现中，在合适的时机将状态存储，即调用 persist()

Notes：为避免内存溢出，Raft 需定期丢弃旧日志，不过这是下个 lab 的内容

Hints:

- 2C 部分的测试集中在 RPC 请求和响应超时上

- 为提升性能，follower 要能批量复制缺失日志

- 整个 lab2 测试的合理用时是 4min，而 CPU 用时应该在 1min 

测试用例先从 TestPersist12C 一步一步来。