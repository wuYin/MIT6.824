---
title: Fault Tolerance. Raft
date: 2019-05-14 09:28:31
tags: 分布式系统
---

原 Lecture：[l-raft.txt](https://pdos.csail.mit.edu/6.824/notes/l-raft.txt)

<!-- more -->

TOPIC：2A

整个课程实现 Raft 的安排：

- Lab 2A, 2B：Raft 的选主和日志复制
- Lab 2C, Lab3：Raft 节点的数据持久化和日志快照

在处理 2A 选主时：

- 超时后开启新一轮选举的机制，可能造成 un-needed 的选举发生，是安全的（网络已隔离，且有一致性检查）
- 旧的 leader 可能在网络分区中依旧存活，并自认为还是 leader

Leader 获胜要立刻发送心跳广而告之，以避免其他选举发生



TOPIC：2B

区分好日志的 replicated 和 committed  状态：

- committed：Raft 能保证 committed 不会消失，会一直留存在 leader 中
- replicated but not committed：可能会被覆盖

Raft 能保证节点日志的最终一致性，而且 commit 机制能确保节点只会执行 stable 的日志。

额外机制：leader 不能通过副本计数来提交之前任期的日志

过期的 servers 收到 AppendEntries 后强制覆写本地 uncommitted 的日志，哪怕这些日志很多。这么做是 ok 的，因为 leader 在日志 committed 后才会响应客户端（否则就是超时）



附录：lab2 的已有方法

`rf.Start(command) (index, term, isLeader)`

- 在 Lab3 中 k/v Servers 在执行 Put/Get 操作时候会调用 `Start()`
- Start() 会在新日志上开始新的 Agreement
- Start() 必须立刻返回，等待日志的 commit 状态转变放到 AppendEntries RPC 的 handler 中去做。（这句话对 Lab2 十分的关键）
- isLeader：如果不是 leader 那 client 应该 retry 其他的节点
- term：leader 的任期，用于 clent 检测过期的 leader
- index：如果日志 commit，那 index 就是它在 leader logs 中的索引



FAQ

>  Q：论文提到 Raft 只能在非拜占庭网络环境下工作，拜占庭错误为什么会使 Raft 发生错误呢？

A：Non-Byzantine 意为节点是 fail-stop 的，要么正确遵循 Raft 协议，要么停机。大多数电源故障都是 Non-Byzantine 的，只会导致节点无法执行指令，此时 Raft 可能不在此节点操作，但不会把错误的结果响应给客户端。拜占庭问题是指某些节点内部因为 bug 或人为原因，导致命令的执行都是错误的。此时 Raft 有可能将错误的结果返回给客户端。



>  Q：如果 client 请求 leader 执行命令，但 leader 还没来得及把此命令复制给所有 followers 就崩溃了，那新 leader 是否含有此命令日志？是否会导致该请求命令的丢失？

A：是的，有可能会丢失。如果某条日志未 committed，那 Raft 将不保证该日志在 leader 变更后继续保存。

日志可能丢失是允许的，如果 Raft 无法 commit 新日志，那 client 就不会收到响应，而是请求超时，超时后 client 会重试请求。

client 能重试请求，就要求系统本身要能处理重复的请求，将在 Lab3 中实现。（论文有提及，每次请求用唯一 id 标识，重复请求保持 id 不变）



> Q：如果选举超时时间过短，会不会造成 Raft 故障？

A：不会。过短的选举超时时间不会影响 Raft 的安全性，只会影响可用性（可能要经过成百上前轮的选举才能选出 leader…）

- 超时时间过短：新选出来的 leader 可能还没来得及发送 AppendEntries RPC 心跳给 followers，它们就自己超时了，这会使 Raft 一直在花时间选 leader，而没时间处理 client 请求。
- 超时时间过长：在可用 leader 选出来前，整个集群额外的不可用时间会变长



> Q：只要 candidate 收到了多数票，就能立马变为 leader 吗？是否要等到所有节点都响应 RequestVote RPC 

是的，仅需过半的选票。长时间等待所有 RPC 返回可能会导致系统出错，因为有的节点失效了不能调用成功。



**下边这几个 QA 的内容在论文中没有细讲，但在实现时十分重要。**

> Q：followers 什么时候将 log entries apply 到自己的状态机？

A：followers 只能等待 leader 发起 AppendEntries RPC 中 leaderCommit 字段，来触发自己的 log entry 状态变为 committed，进而 execute/apply 日志。



> Q：leader 是否需要等待所有 AppendEntries RPC 返回？

A：leader 应并发地调用 AppendEntries RPC 请求且不用等待调用返回，它只需要在后台对成功复制副本计数，若成功复制到大多数节点，则将日志标记为 committed 状态。leader 的并发Append 可遵循如下模式：

```go
  for each server {
    go func() {
      // send the AppendEntries RPC and wait for the reply
      if reply.success == true {
        // increment count
        if count == nservers/2 + 1 {
          // this entry is committed
        }
      }
    } ()
  }
```



> Q：为什么 Raft 节点的日志索引要从 1 开始？

A：你应该将 index 为 1 的索引视为第一条日志，而 index 为 0 处的日志 term 为 0，方便后序日志进行一致性检查。