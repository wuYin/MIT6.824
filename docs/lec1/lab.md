---
title: Lab MapReduce
date: 2019-03-31 20:38:42
tags: Golang
---

Lab1: 模拟实现 MapReduce

<!-- more -->



## 介绍

在本实验中，你将使用 Golang 构建前文中描述的 MapReduce 库并实现一个容错的分布式系统。第一节需实现简单的 MR 程序，第二节需要实现 master 来分发任务给 worker 并处理 wroker 的失效情形。

实验环境：Go1.9，x86，环境提供和单机版和分布式版本 MR 的大部分实现。在单机版本中，只有等所有 map worker 都运行完毕后才开始逐一运行 reduce worker，容易调试但效率不高。在分布式版本中，多个 map worker 会并行执行，随后再并行执行多个 reduce worker，运行很快但不好实现和调试。



## 开始：熟悉原有代码

mapreduce pkg 已在 mapreduce 目录下实现了一个简单的 MR 库，调用程序可直接调用 master.go 中的 `Distributed()` 来开始执行任务，也可以调用 `Sequential()` 来顺序执行并调试。

#### 每个任务的执行流程：

- 1.准备输入和自定义函数

  调用程序需提供输入文件的编号、map 函数实现、reduce 函数，以及 reduce 任务的数量(nReduce)

- 2.启动 RPC Server 等待 worker 注册

  等这些预备工作做完后，master 会启动一个 RPC Server (master_rpc.go) 并等待 worker 的注册 （使用 master.go 中的 `Register()` 函数），当有任务需要运行时（见第 4/5 步），schedule.go 中的 `schedule()` 会分配这些任务给 worker 处理并处理 worker 失效

- 3.执行 Map 任务

  master 会将每个输入文件处理成一个 map 任务，并对每个任务至少调用一次 common_map.go 中的 `doMap()` 函数，随后直接调用 `Sequential()` 直接运行，或调用 worker.go 中的 `DoTask` RPC

  doMap 每次都会读取适量的文件内容并调用 Map 函数处理，处理结果会以 k-v pairs 的形式写入到 nReduce 个临时文件中，doMap 会将 key 哈希到某个临时文件中，这样 reduce 才能通过 key 找到 pairs，当所有 map 任务执行完毕后会生成 nMap*nReduce 个临时文件，每个文件的文件名由固定的 prefix，map task 编号，reduce task 编号组成。比如有 2 个 map 任务和 3 个 reduce 任务，那会生成如下临时文件：

  ```
  mrtmp.xxx-0-0
  mrtmp.xxx-0-1
  mrtmp.xxx-0-2
  mrtmp.xxx-1-0
  mrtmp.xxx-1-1
  mrtmp.xxx-1-2
  ```

  每个 worker 必须能像读输入文件一样读取其他 worker 的输出，实际部署过程中会使用像 GFS 的分布式文件系统来保证跨主机的两个 worker 间相互读取是没问题的，不过在本实验中始终在一台主机上运行所有 worker 并共享本地文件系统。

- 4.执行 Reduce 任务

  接下来 master 会在每个 reduce worker 上至少调用一次 common_reduce.go 中实现的 `doReduce()` 函数，和 doMap 一样它也能直接运行或通过 RPC 运行。

  编号为 r 的 reduce task 会收集每个 map task 的第 r 个临时输出文件，随后调用 Reduce 函数来逐一处理出现在这些文件中的 key

  这些 reduce tasks 最后会生成 nReduce 个最终文件

- 5.merge 结果

  最后，master 会调用 master_splitmerge.go 中的 `merge()` 函数，用于合并 nReduce 个最终文件的内容

- 6.关闭系统

  任务执行结束后，master 会给所有 worker 发起停机 RPC 调用，随后关闭 RPC Server

> 剩下的 2 个练习部分，你必须
>
> - 修改或实现 doMap、doReduce、schedule 函数，分别位于 common_map.go、common_reduce.go、schedule.go 文件
> - 在 main/wc.go 中实现 map 函数和 reduce 函数的逻辑



### 1. 处理 Map/Reduce 的输入输出

你需要实现 doMap 和 doReduce，以及 sequential 的逻辑。原有代码还缺少 2 个重要过程：

- 将输出分流为 map task 的函数
- 将所有 input 聚集到 reduce task 的函数

map task 会被 common_map.go 中的 doMap 执行，reduce task 会被 common_reduce.go 中的 doReduce 执行，参考注释补全实现。为了检查实现是否正确，在 test_test.go 中有多个测试样例：

```shell
$ go test -run Sequential
ok  	mapreduce	2.694s
```

实验笔记：第一节只需要完成 doMap 和 doReduce 的实现，在 doMap 中读取指定输入文件，并调用 mapFunc 来进行 split 操作，split 的 k-v 会被 hash mod 后均匀地分散到 nReduce 个文件中。这里要注意 mapFunc 和 reduceFunc 之间的数据要约定好 encode 和 decode，可以批量 marshal，也可以按行 encode



### 2. 单机版 word count

本节中你将实现一个 MR 的实际应用：单机版 word count 程序，在 main/wc.go 文件中需实现 mapF 和 reduceF 函数来从输入中对单词计数，在 main/ 文件夹下有一些测试文本文件：`pg-*.txt`，内容是从 [Project Gutenberg](https://www.gutenberg.org/ebooks/search/%3Fsort_order%3Ddownloads) 直接下载的。

实验笔记：直接参考 paper 流程图即可。



### 3. 分布式版 MR tasks

截止目前的 MR 版本同时只能执行一个 task，然而 MR 最大的卖点在于开发者能轻松将顺序执行的代码并行化，本节实现的 MR 将使用多核 CPU 将多个 task 分配在给多线程运行，真正实现了并行化。和真正部署到生产环境中的多机实例不同，本节的实现使用 RPC 模拟分布式执行过程。

master.go 的代码完成了大部分管理一个 MR job 的工作，同时已在 worker.go 中提供了完整的 worker 代码，在 common_rpc.go 中实现了 RPC 调用的函数。

你要做的事情就是实现 schedule.go 中的 schedule 函数，master 会分别在 map 阶段、reduce 阶段分别调用一次 schedule，它负责将 task 分发给闲置的 wroker 处理，通常 task 的数量是要比 worker 线程数量多的，所以 schedule 需给每个 worker 分配一系列的 task，随后等待所有 task 都处理完毕它才返回。

schedule 可读取 registerChan 来直到已注册的 worker  RPC 调用地址。

schedule 向 worker 发起一个 Wroker.DoTask 的 RPC 调用来告知它要执行一个新 task，调用参数定义为 DoTaskArgs 类型，其中 File 参数只是给 map wroker 用于第一步读取的，所有输入文件都记录在 schedule.mapFiles

调用 common_rpc.go 中定义好的 call() 函数来向 worker 发起 RPC 调用：

```go
call func(srv string, rpcname string, args interface{}, reply interface{}) bool 
```

第一个参数是 worker 的网络地址，是从 registerChan 中读取的。第二个参数是  `"Worker.DoTask"`，第三个参数就是 DoTaskArgs 入参结构了。

第三部分的实现应该只修改 schedule.go，完成后使用 `go test -run TestParallel` 来进行单元测试。

实验笔记：schedule 负责并发地调度已注册的 workers 来批量并发地执行 map task，或者批量执行 reduce tasks，每个 worker 注册时会通知 registerChan，所以处理 map tasks 时需将 workers 的地址暂存为全局变量，随后 reduce tasks 处理时候也能复用闲置的 workers



### 4. 处理 worker 失效

本节主要实现让 master 能处理 workers 的失效场景，因为 worker 本身没有持久化的状态，所以 MR 在 worker 处理 task 失败后 call() 调用会返回 false 调用超时的返回值，失败的 task 应该被重新调度执行。

注意，RPC 调用失败不代表 task 没有被执行，有可能是 worker 执行完了但是没有返回丢包，又或是的确是 task 的执行超时了。超时 task 会被重新运行，因此可能存在 2 个 worker 接收到同一任务，执行并生成输出。MR 要求用户自定义的 map / reduce 必须是 functional 函数独立的，即相同的输入必定返回相同的输出，即 task 执行的结果是确定的。

MR 保证了函数输出是原子性的，即 task 的输出要不不写入文件，要么全部写入文件 （其实 lab 的代码没有实现原子写入，但限制了每个输出文件只会有单个 worker 会写入，所以不会发生多worker 同时写入同一文件的情况）

> 注明：在 lab1 中暂不考虑 master 的容错，因为 master 保存了各个 worker 的状态，当从崩溃的 master 中恢复时必须将这些状态全部恢复，显然还需要加入很多机制来保证，在后边的 lab 中将看到怎么保证 master 的容错。

你的实现必须能通过 test_test.go 中的剩余两个单元测试。case1会触发一个 worker 不可用。case2 会测试多个 worker 不可用的情况，同时定期启动新的 worker 注册到 master，只不过这些 worker 在处理几个 task 后会自动失效。第四节的作业同样在 schedule.go 中修改。

实验笔记：第四节搞了两三个小时。。。做了些尝试，总结出几个点：

- 锁使用粒度要小，要集中

  一把锁不要用得太分散，比如在某个函数中 A B C 三个地方都用到了，那极有可能在 goroutine1 的 A 区域刚把锁释放，goroutine 2就把锁抢走了，如果还要等待 goroutine 1 释放共享资源，结果 goroutine 1 在 B 区域等待锁，相互等待造成死锁。

- 不要通过共享内存来进行通信，而是通过通信来共享内存

  代码已有的 registerChan 就是无缓冲 channel，不能为了循环利用 worker 就直接复用 registerChan，那 map tasks 执行完毕后向无缓冲 channel 发送数据直接就阻塞了。这时候使用缓冲 channel 来在各个 task goroutine 之间共享可用的 worker 就很方便。

- 不要写多个 goroutine 可能会产生竞态的代码

  如果跑测试有时候通过，有时候在 lock 周围的代码 panic，那可能代码中还隐藏得有竞态代码，这种代码不好复现调试。并发代码不要滥用 channel 和 sync.Mutex，梳理好多个 goroutine 之间数据传递方式后再写代码也不迟。



### 5. 生成倒排索引（可选）

倒排索引在计算机科学领域应用十分广泛，尤其是在文档搜索最为常用，它可简单理解为一个key是关键字，value 是包含 key 出现过的文档数据的 map

在 main/ii.go 中已有代码原型，你需要实现 mapF 和 reduceF 函数来生成倒排索引，如：

```shell
$ go run ii.go master sequential pg-*.txt
$ head -n5 mrtmp.iiseq
A: 8 pg-being_ernest.txt,pg-dorian_gray.txt,pg-frankenstein.txt,pg-grimm.txt,pg-huckleberry_finn.txt,pg-metamorphosis.txt,pg-sherlock_holmes.txt,pg-tom_sawyer.txt
ABOUT: 1 pg-tom_sawyer.txt
ACT: 1 pg-being_ernest.txt
ACTRESS: 1 pg-dorian_gray.txt
ACTUAL: 8 pg-being_ernest.txt,pg-dorian_gray.txt,pg-frankenstein.txt,pg-grimm.txt,pg-huckleberry_finn.txt,pg-metamorphosis.txt,pg-sherlock_holmes.txt,pg-tom_sawyer.txt
```

即哪些单词分别出现在了哪些文档过：

```
word: #documents documents,sorted,and,separated,by,commas
```

