---
title: Lec2. RPC 和多线程基础
date: 2019-04-02 21:36:02
tags: lec2
---

介绍 RPC 和多线程基础知识，只对重要部分做些翻译。原文：[l-rpc.txt](https://pdos.csail.mit.edu/6.824/notes/l-rpc.txt)

<!-- more -->

#### 使用多线程的一些挑战

共享数据：当线程 A 读取共享数据时线程 B 可能正在更新，比如 2 个线程都想执行 `count = count + 1`，此处会产生竞态条件，这种隐藏有竞态条件的程序在高并发场景下会存在 bug，还不易调试。

消除竞态条件：

- 使用锁 Mutex，或 sync pkg 下的其他同步手段如 Cond
- 不共享数据，隔离数据能彻底保证安全

goroutine 之间如何通信：

- 使用 sync.WaitGroup 保证同步
- 使用 channel 进行通信，从而共享内存数据

并发粒度：

- 粗粒度：尽量少且简单，对并发/并行友好，比如直接把整个闭包当做 goroutine 运行
- 细粒度：并发性能更好，同时也可能会造成更多竞态和死锁



### 网络爬虫的一些要点

- 并发 IO 操作：同一时间请求多个 URL

- URL 去重：不要重复爬取同一 URL

在 notes/crawler.go 中有 3 种 golang.org 的 fake 网页爬虫实现，分别是：

#### 1. 串行爬取

通过使用 fetched map 来保存已爬取过的网页，避免重复爬取和子页面环形嵌套。虽然一个递归和 map 就搞定了，但同一时间只能处理 1 个子 URL 

#### 2. Mutex 控制的并发爬取

对每个子 URL 都开一个新 goroutine 单独处理，充分利用网络 IO 的阻塞时间达到并发目的。注意多个 goroutine 共享同一个变量 fetched map，使用 lock 避开竞态读写。使用计数器 WaitGroup 等待所有子 URL 爬取完毕。

#### 3. Channel 共享的并发爬取

channel：无缓冲 channel 在发送和接收操作上都是阻塞的，适合用于 goroutine 间同步数据。如果 goroutine 在向无缓冲 channel 上收发数据或在空/满的缓冲 channel 上收/发数据的同时还持有 lock，很有可能间接造成其他抢锁的 goroutine 阻塞。

注意 master 中去重的 fetched map 在读写时并没有进行 lock 保护，因为它是独享的，不存在竞态条件。

> 若多个 goroutine 间共享的是状态，一般使用 lock 比较好。如果共享的是通信数据，那 channel 更好



### RPC: Remote Procedure Call (远程过程调用)

RPC 是分布式系统中比较关键的部分，用于 client/server 模式下的通信。调用过程：

```
  Client             Server
    request--->
       <---response
```

RPC 的调用像在本地一样：

```
Client:
z = fn(x, y)

Server:
fn(x, y) {
  compute
  return z
}
```

调用层级：

```
  client app         handlers
    stubs           dispatcher
   RPC lib           RPC lib
     net  ------------ net
```

RPC 可简单理解为函数调用网络化，Client 在本地调用的只是一个值为 nil 但函数签名一致的函数，真正调用时通过网络传递给 Server，返回后再打包生成返回值。



### 使用 RPC 实现的简单 k/v 存储系统

几个值得注意的点：

- 对于 Get Put 操作都是要上锁的，不然 rpc lib 对每个连接都直接 dispatch goroutine 去处理，在高并发场景下很有可能旧的缓存数据因为网络延迟高后 Put，进而导致 Get 的缓存是旧的。
- rpc 标准库在传递指针数据时会先取值再打包，不能传递 channel、function 等类型的数据

#### RPC 调用失败的情况：

网络丢包，网络中断，Server 负载过高导致执行超时，Server panic 崩溃等

#### Client 如何处理调用失败：

- Call() 调用自己设置超时时间，比如石油 time.After 等
- 重试一定次数的请求后返回 error

#### RPC 良好实践

- 如果是读请求，即幂等的调用执行多少次都可以

- 如果是写请求，那 Server 端要检测并拦截掉 Client 重试的写请求。检测机制：每个 Client 都有自己的 id，每个请求都有自己的 xid，重试的请求复用 xid 即可，这样 Server 就能根据 xid 去重写请求。Server 伪代码：

  ```go
  if seen[xid]:
    r = old[xid]
  else
    r = handler()
    old[xid] = r
    seen[xid] = true
  ```



那 Server 如何保证每个写请求最多执行一次？在 lab3 中还会遇到同样的问题，即 Client 如何保证 xid 不重复？使用随机 big number + clientid (ip地址) 会有冲突的可能性。文中写了好几个 idea，但都不能保障 Server 失效重启后对于每个请求 at-most-once 执行的容错性，lab3 有讨论。