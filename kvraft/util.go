package raftkv

import (
	"fmt"
	"log"
	"strings"
	"time"
)

// Debugging
const debug = false

type LogWriter struct{}

func (w LogWriter) Write(bytes []byte) (int, error) {
	return fmt.Print(time.Now().Format("05.999"), "\t", string(bytes))
}

func pr(format string, vs ...interface{}) {
	log.SetFlags(0)
	log.SetOutput(new(LogWriter))
	if !strings.HasSuffix(format, "\n") {
		format += "\n"
	}
	if debug {
		log.Printf(format, vs...)
	}
}
