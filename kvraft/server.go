package raftkv

import (
	"MIT6.824/labgob"
	"MIT6.824/labrpc"
	"MIT6.824/raft"
	"fmt"
	"sync"
	"time"
)

type Op struct {
	Cid   int64  // client id
	Seq   int32  // request sequence number
	Cmd   string // command type, Put/Append/Get
	Key   string
	Value string
}

func isSameOp(a, b Op) bool {
	return a.Cid == b.Cid && a.Seq == b.Seq && a.Cmd == b.Cmd && a.Key == b.Key && a.Value == b.Value
}

func (op Op) String() string {
	return fmt.Sprintf("cid: %d|seq: %d|%s|%s : %q", op.Cid, op.Seq, op.Cmd, op.Key, op.Value)
}

type KVServer struct {
	mu        sync.Mutex
	me        int
	rf        *raft.Raft
	persister *raft.Persister
	applyCh   chan raft.ApplyMsg

	maxraftstate int               // snapshot if log grows this big
	db           map[string]string // kvDB
	cid2seq      map[int64]int32   // client id to max request sequential number
	agreeChs     map[int]chan Op   // command index to op channel
	killCh       chan struct{}     // kill KVServer
}

func (kv *KVServer) Get(args *GetArgs, reply *GetReply) {
	cmd := Op{Cmd: "Get", Key: args.Key,}
	idx, _, isLeader := kv.rf.Start(cmd)
	if !isLeader {
		reply.WrongLeader = true
		return
	}

	ch := kv.getAgreeCh(idx)
	var op Op
	select {
	case op = <-ch:
		close(ch)
	case <-time.After(500 * time.Millisecond):
		reply.WrongLeader = true
		return
	}
	if !isSameOp(cmd, op) {
		reply.WrongLeader = true
		return
	}

	kv.mu.Lock()
	reply.Value = kv.db[args.Key]
	kv.mu.Unlock()

	// pr("Server|%d|Get|SUCC|%s -> %q", kv.me, args.Key, reply.Value)
	return
}

func (kv *KVServer) PutAppend(args *PutAppendArgs, reply *PutAppendReply) {
	cmd := Op{
		Cid:   args.Cid,
		Seq:   args.Seq,
		Cmd:   args.Op,
		Key:   args.Key,
		Value: args.Value,
	}
	idx, _, isLeader := kv.rf.Start(cmd)
	if !isLeader {
		reply.WrongLeader = true
		return
	}

	ch := kv.getAgreeCh(idx) // sequence of PutAppend() and <-applyCh are uncertain
	var op Op
	select {
	case op = <-ch:
		close(ch)
	case <-time.After(500 * time.Millisecond):
		reply.WrongLeader = true
		return
	}

	// if old leader has in net partition, may it's log may be overwrited, then reply value will be different
	if !isSameOp(cmd, op) {
		reply.WrongLeader = true
		return
	}

	// pr("Server|%d|%s|SUCC|%s -> %q", kv.me, args.Op, args.Key, args.Value)
}

func (kv *KVServer) Kill() {
	kv.rf.Kill()
	kv.killCh <- struct{}{}
}

func StartKVServer(servers []*labrpc.ClientEnd, me int, persister *raft.Persister, maxraftstate int) *KVServer {
	labgob.Register(Op{})

	kv := new(KVServer)
	kv.me = me
	kv.maxraftstate = maxraftstate
	kv.persister = persister

	kv.applyCh = make(chan raft.ApplyMsg)
	kv.rf = raft.Make(servers, me, persister, kv.applyCh)
	kv.db = make(map[string]string)
	kv.cid2seq = make(map[int64]int32)
	kv.agreeChs = make(map[int]chan Op)
	kv.killCh = make(chan struct{})

	// kv server restore the snapshot from the persister when it re-starts
	snapshot := kv.persister.ReadSnapshot()
	kv.db, kv.cid2seq = kv.decodeSnapshot(snapshot)
	// pr("KVServer:%d|re-start|db: %v|cid2req: %v", kv.me, len(kv.db), len(kv.cid2seq))

	go kv.waitAgree()

	return kv
}

func (kv *KVServer) waitAgree() {
	for {
		select {
		case <-kv.killCh:
			return
		case msg := <-kv.applyCh:
			if !msg.CommandValid { // snapshot data
				buf := msg.Command.([]byte)
				kv.mu.Lock()
				kv.db, kv.cid2seq = kv.decodeSnapshot(buf)
				kv.mu.Unlock()
				continue
			}

			op := msg.Command.(Op)
			// pr("Server|%d|Agreed|%s", kv.me, op)

			kv.mu.Lock()
			maxSeq, ok := kv.cid2seq[op.Cid]
			if !ok || op.Seq > maxSeq {
				kv.cid2seq[op.Cid] = op.Seq
				switch op.Cmd {
				case "Put":
					kv.db[op.Key] = op.Value
				case "Append":
					kv.db[op.Key] += op.Value
				}
			}
			kv.checkSnapshot(msg.CommandIndex) // use committed index as LastIncludedIndex
			kv.mu.Unlock()

			kv.getAgreeCh(msg.CommandIndex) <- op
		}
	}
}

func (kv *KVServer) checkSnapshot(appliedId int) {
	if kv.maxraftstate == -1 {
		return
	}

	// take snapshot when raft size come near upper limit
	if kv.persister.RaftStateSize() < kv.maxraftstate*9/10 {
		return
	}

	// pr("Server:%d|TakeSnapshot|START", kv.me)
	rawSnapshot := kv.encodeSnapshot()
	go kv.rf.TakeSnapshot(appliedId, rawSnapshot)
	// pr("Server:%d|TakeSnapshot|END", kv.me)
}

func (kv *KVServer) getAgreeCh(idx int) chan Op {
	kv.mu.Lock()
	defer kv.mu.Unlock()

	ch, ok := kv.agreeChs[idx]
	if !ok {
		ch = make(chan Op, 1) // never block this
		kv.agreeChs[idx] = ch
	}
	return ch
}
