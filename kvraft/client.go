package raftkv

import (
	"MIT6.824/labrpc"
	"crypto/rand"
	"math/big"
	"sync/atomic"
	"time"
)

type Clerk struct {
	servers []*labrpc.ClientEnd // kv servers / raft peers
	leader  int                 // latest known leader
	cid     int64               // client id
	seq     int32               // latest request seq num
}

func nrand() int64 {
	max := big.NewInt(int64(1) << 62)
	bigx, _ := rand.Int(rand.Reader, max)
	x := bigx.Int64()
	return x
}

func MakeClerk(servers []*labrpc.ClientEnd) *Clerk {
	ck := new(Clerk)
	ck.servers = servers
	ck.seq = 0
	ck.cid = nrand()
	return ck
}

func (ck *Clerk) Get(key string) string {
	for {
		args := GetArgs{Key: key}
		var reply GetReply
		ok := ck.servers[ck.leader].Call("KVServer.Get", &args, &reply)

		// if timeout or wrong leader, change next server
		if !ok || reply.WrongLeader {
			// pr("Client|Get|Timeout|%s", reply.Err)
			ck.changeLeader()
			continue
		}

		return reply.Value
	}
}

func (ck *Clerk) PutAppend(key string, value string, op string) {
	curSeq := atomic.AddInt32(&ck.seq, 1)
	for {
		args := PutAppendArgs{
			Cid:   ck.cid,
			Seq:   curSeq,
			Key:   key,
			Value: value,
			Op:    op,
		}
		var reply PutAppendReply
		ok := ck.servers[ck.leader].Call("KVServer.PutAppend", &args, &reply)

		if !ok || reply.WrongLeader {
			// pr("Client|PutAppend|Timeout|%v|%+v", ok, reply)
			ck.changeLeader()
			continue // retry
		}

		return
	}
}

func (ck *Clerk) Put(key string, value string) {
	ck.PutAppend(key, value, "Put")
}

func (ck *Clerk) Append(key string, value string) {
	ck.PutAppend(key, value, "Append")
}

// change leader
func (ck *Clerk) changeLeader() {
	ck.leader++
	ck.leader %= len(ck.servers)
	time.Sleep(50 * time.Millisecond)
}
