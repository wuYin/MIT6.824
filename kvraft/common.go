package raftkv

import (
	"bytes"
	"encoding/gob"
	"fmt"
)

const (
	OK        = ""
	ErrExpReq = "ErrExpReq"
)

type Err string

// Put or Append
type PutAppendArgs struct {
	Cid   int64 // client id
	Seq   int32 // request sequential number
	Key   string
	Value string
	Op    string // "Put" or "Append"
}

type PutAppendReply struct {
	WrongLeader bool
	Err         Err
}

type GetArgs struct {
	Key string
}

type GetReply struct {
	WrongLeader bool
	Err         Err
	Value       string
}

func (kv *KVServer) encodeSnapshot() []byte {
	w := new(bytes.Buffer)
	e := gob.NewEncoder(w)
	if err := e.Encode(kv.cid2seq); err != nil {
		panic(fmt.Errorf("encode cid2seq fail: %v", err))
	}
	if err := e.Encode(kv.db); err != nil {
		panic(fmt.Errorf("encode db fail: %v", err))
	}
	return w.Bytes()
}

func (kv *KVServer) decodeSnapshot(buf []byte) (map[string]string, map[int64]int32) {
	db := make(map[string]string)
	cid2seq := make(map[int64]int32)

	if buf == nil || len(buf) == 0 {
		return db, cid2seq
	}
	r := bytes.NewReader(buf)
	e := gob.NewDecoder(r)
	if err := e.Decode(&cid2seq); err != nil {
		panic(fmt.Errorf("decode cid2seq fail: %v", err))
	}
	if err := e.Decode(&db); err != nil {
		panic(fmt.Errorf("decode db fail: %v", err))
	}
	return db, cid2seq
}
