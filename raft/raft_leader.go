package raft

import "time"

// if rf is leader, start the agreement and return immediately
//
// the first return value is the index that the command will appear at
// if it's ever committed. the second return value is the current
// term. the third return value is true if this server believes it is
// the leader.
//
func (rf *Raft) Start(command interface{}) (int, int, bool) {
	if !rf.isRunningLeader() {
		return -1, -1, false
	}

	rf.mu.Lock() // avoid client concurrent submit commands and lost some of them
	curTerm := rf.curTerm
	e := LogEntry{Term: rf.curTerm, Command: command}
	rf.logs = append(rf.logs, e)
	rf.persistStates()
	last := rf.lastIdx()
	rf.matchIndex[rf.me] = last
	rf.nextIndex[rf.me] = last + 1 // replicated, but not committed, so don't update commit index in here
	rf.mu.Unlock()

	// pr("START|Command:%v -> Leader:%d, Term:%d", command, rf.me, rf.curTerm)

	// start agreement/sync
	for i := range rf.peers {
		if i == rf.me {
			continue
		}
		rf.syncConds[i].Broadcast()
	}
	return last, curTerm, true
}

// send heartbeat
func (rf *Raft) heartbeat() {
	ch := time.Tick(HEARTBEAT_INTERVAL)
	for {
		if !rf.isRunningLeader() {
			return
		}

		for i := range rf.peers {
			if i == rf.me {
				rf.mu.Lock()
				rf.resetElectTimer() // leader reset timer similar as other servers receive AppendEntries RPC
				rf.mu.Unlock()
				continue
			}

			rf.syncConds[i].Broadcast()
		}
		<-ch
	}
}

// leader sync logs to followers
func (rf *Raft) sync() {
	for i := range rf.peers {
		if i == rf.me {
			continue
		}

		go func(server int) {
			for {
				if !rf.isRunningLeader() {
					return
				}

				rf.mu.Lock()
				rf.syncConds[server].Wait() // wait for trigger

				// sync new log or missing logs to server
				next := rf.nextIndex[server]

				// if follower far behind from leader, just send snapshot to it for speeding up replay
				if next <= rf.lastIncludedIndex {
					rf.syncSnapshot(server)
					continue
				}

				args := AppendEntriesArgs{
					Term:         rf.curTerm,
					LeaderID:     rf.me,
					Entries:      nil,
					LeaderCommit: rf.commitIndex,
				}
				if next < rf.logLength() { // logs need sync
					args.PrevLogIndex = next - 1
					args.PrevLogTerm = rf.getLog(next - 1).Term
					args.Entries = append(args.Entries, rf.logs[rf.subIdx(next):]...)
					// pr("Sync|Logs|%s:%d -> Server:%d|%+v", rf.state, rf.me, server, args)
				}
				rf.mu.Unlock()

				// do not depend on labrpc to call timeout(it may more bigger than heartbeat), so should be check manually
				var reply AppendEntriesReply
				respCh := make(chan struct{})
				go func() {
					if rf.sendAppendEntries(server, &args, &reply) {
						respCh <- struct{}{}
					}
				}()
				select {
				case <-time.After(RPC_CALL_TIMEOUT): // After() with currency may be inefficient
					// // pr("Sync|Timeout|Leader:%d, Term:%d -/-> Server:%d", rf.me, rf.curTerm, server)
					continue
				case <-respCh:
					close(respCh)
					// // pr("Sync|Resp|Server:%d, Succ:%v, Term:%d ---> %v", server, reply.Succ, reply.Term, rf)
				}

				if !reply.Succ {
					rf.mu.Lock()
					if reply.Term > rf.curTerm {
						rf.back2Follower(reply.Term, VOTE_NIL)
						rf.mu.Unlock()
						return
					}
					if rf.state != Leader || reply.Term < rf.curTerm { // curTerm changed already
						rf.mu.Unlock()
						return
					}
					rf.mu.Unlock()

					// consistency check failed, use conflict info to back down server's nextIndex
					if reply.ConflictIndex > 0 {
						// pr("Sync|Conflict|%s:%d, Entries:%v -/-> Server:%d, Conflict:%d, Term:%d", rf.state, rf.me, args.Entries, reply.ConflictIndex, reply.ConflictTerm)
						rf.mu.Lock()
						firstConflict := reply.ConflictIndex
						if reply.ConflictTerm != NIL_TERM {
							lastIdx := rf.lastIdx()
							for i := rf.addIdx(0); i <= lastIdx; i++ {
								if rf.getLog(i).Term != reply.ConflictTerm {
									continue
								}
								for i <= lastIdx && rf.getLog(i).Term == reply.ConflictTerm {
									i++
								}
								// set nextIndex to be the one beyond the index of the last entry in that term in its log.
								firstConflict = i
								break
							}
						}
						rf.nextIndex[server] = firstConflict
						rf.mu.Unlock()
					}
					continue
				}

				// append succeed
				if len(args.Entries) > 0 {
					// dNext, dMatch := rf.nextIndex[server], rf.matchIndex[server]
					rf.mu.Lock()
					rf.matchIndex[server] = args.PrevLogIndex + len(args.Entries) // conservative measurement of log agreement
					rf.nextIndex[server] = rf.matchIndex[server] + 1              // just guess for agreement, maybe move backwards
					rf.updateCommitIndex()
					rf.mu.Unlock()
					// pr("Sync|Succ|%s:%d, Entries:%v ---> Server:%v|Next:%d->%d, Match:%d->%d", rf.state, rf.me, args.Entries, server, dNext, rf.nextIndex[server], dMatch, rf.matchIndex[server])
				} else {
					// pr("Heartbeat|Succ|%s:%d ---> Server:%v", rf.state, rf.me, server)
				}
			}
		}(i)
	}
}

// once leader replicate logs successfully, check commitIndex > lastApplied
func (rf *Raft) updateCommitIndex() {
	n := len(rf.peers)
	matched := make([]int, n)
	copy(matched, rf.matchIndex)
	revSort(matched)
	if N := matched[n/2]; N > rf.commitIndex && rf.getLog(N).Term == rf.curTerm {
		rf.commitIndex = N
		rf.checkApply()
	}
}

// leader take snapshot should be async like Start(), must return quickly
func (rf *Raft) TakeSnapshot(appliedId int, rawSnapshot []byte) {
	rf.mu.Lock()
	defer rf.mu.Unlock()

	// lock competition may delayed snapshot call, check this otherwise rf.logs[0] may out of bounds
	if appliedId <= rf.lastIncludedIndex {
		return
	}

	// discard the entries before that index, preserved it for AppendEntries consistency check
	rf.logs = rf.logs[rf.subIdx(appliedId):]
	rf.lastIncludedIndex = appliedId
	rf.lastIncludedTerm = rf.logs[0].Term
	rf.persistStatesAndSnapshot(rawSnapshot)
}

// sync snap shot to follower server
// rf.mu is locked when call syncSnapshot()
func (rf *Raft) syncSnapshot(server int) {
	if rf.state != Leader || rf.crashed {
		rf.mu.Unlock()
		return
	}

	args := InstallSnapshotArgs{
		Term:              rf.curTerm,
		LeaderId:          rf.me,
		LastIncludedIndex: rf.lastIncludedIndex,
		LastIncludedTerm:  rf.lastIncludedTerm,
		Data:              rf.persister.ReadSnapshot(),
	}
	rf.mu.Unlock()

	var reply InstallSnapshotReply
	respCh := make(chan struct{})
	go func() {
		if ok := rf.sendInstallSnapshot(server, &args, &reply); ok {
			respCh <- struct{}{}
		}
	}()
	select {
	case <-time.After(RPC_CALL_TIMEOUT):
		return
	case <-respCh:
		close(respCh)
	}

	rf.mu.Lock()
	defer rf.mu.Unlock()
	if reply.Term > rf.curTerm {
		rf.back2Follower(reply.Term, VOTE_NIL)
		return
	}
	if rf.state != Leader || reply.Term < rf.curTerm { // curTerm changed already
		return
	}

	rf.matchIndex[server] = args.LastIncludedIndex
	rf.nextIndex[server] = args.LastIncludedIndex + 1
}
