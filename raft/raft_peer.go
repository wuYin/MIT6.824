package raft

import (
	"MIT6.824/labgob"
	"bytes"
	"fmt"
	"math/rand"
	"time"
)

type PeerState string

const (
	Leader    PeerState = "Leader"
	Candidate           = "Candidate"
	Follower            = "Follower"

	STAY_TERM = -1
	NIL_TERM  = -1 // use for identify missing logs (treat same as conflict logs)
)

func (rf *Raft) isRunningLeader() bool {
	rf.mu.RLock()
	defer rf.mu.RUnlock()
	return rf.state == Leader && !rf.crashed
}

// if RPC request or response contains term T > currentTerm
// set currentTerm = T, convert to follower
func (rf *Raft) back2Follower(higherTerm int, peer int) {
	rf.state = Follower
	if higherTerm != STAY_TERM {
		rf.curTerm = higherTerm
	}
	rf.votedFor = peer
	rf.persistStates()
}

func (rf *Raft) getLog(i int) LogEntry {
	return rf.logs[i-rf.lastIncludedIndex]
}

func (rf *Raft) addIdx(i int) int {
	return rf.lastIncludedIndex + i
}

func (rf *Raft) subIdx(i int) int {
	return i - rf.lastIncludedIndex
}

func (rf *Raft) lastIdx() int {
	return rf.lastIncludedIndex + len(rf.logs) - 1
}

func (rf *Raft) lastTerm() int {
	return rf.logs[len(rf.logs)-1].Term
}

func (rf *Raft) logLength() int {
	return rf.lastIdx() + 1
}

const (
	HEARTBEAT_INTERVAL = 100 * time.Millisecond // most 10 rpc per second
	ELECT_TIMEOUT_BASE = 400 * time.Millisecond // must elect a new leader in 5 seconds
	ELECT_TIMEOUT_GAP  = 100
	RPC_CALL_TIMEOUT   = HEARTBEAT_INTERVAL // tolerate just 100ms to call timeout
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func randElectTime() time.Duration {
	gap := time.Duration(rand.Intn(ELECT_TIMEOUT_GAP)*5) * time.Millisecond
	return ELECT_TIMEOUT_BASE + gap
}

type RaftTimer struct {
	d time.Duration
	t *time.Timer
}

func (rf *Raft) initElectTimer() {
	d := randElectTime()
	rf.timer = &RaftTimer{d: d, t: time.NewTimer(d)}
	return
}

func (rf *Raft) resetElectTimer() {
	rf.timer.d = randElectTime()
	rf.timer.t.Reset(rf.timer.d)
}

func (t *RaftTimer) String() string {
	return fmt.Sprintf("%dms", t.d.Nanoseconds()/int64(time.Millisecond))
}

// persist raft state to stable storage
func (rf *Raft) persistStates() {
	w := bytes.NewBuffer(nil)
	e := labgob.NewEncoder(w)
	e.Encode(rf.curTerm)
	e.Encode(rf.votedFor)
	e.Encode(rf.lastIncludedIndex)
	e.Encode(rf.lastIncludedTerm)
	e.Encode(rf.logs)
	data := w.Bytes()
	rf.persister.SaveRaftState(data)
}

// restore previously persisted state
func (rf *Raft) readPersist(data []byte) {
	if data == nil || len(data) < 1 { // bootstrap without any state?
		return
	}

	r := bytes.NewReader(data)
	d := labgob.NewDecoder(r)
	var curTerm, votedFor, lastIncludedIndex, lastIncludedTerm int
	var logs []LogEntry
	if err := d.Decode(&curTerm); err != nil {
		fmt.Println("decode fail:", err)
	}
	if err := d.Decode(&votedFor); err != nil {
		fmt.Println("decode fail:", err)
	}
	if err := d.Decode(&lastIncludedIndex); err != nil {
		fmt.Println("decode fail:", err)
	}
	if err := d.Decode(&lastIncludedTerm); err != nil {
		fmt.Println("decode fail:", err)
	}
	if err := d.Decode(&logs); err != nil {
		fmt.Println("decode fail:", err)
	}

	rf.mu.Lock()
	rf.curTerm, rf.votedFor, rf.logs = curTerm, votedFor, logs
	rf.lastIncludedIndex, rf.lastIncludedTerm = lastIncludedIndex, lastIncludedTerm
	rf.mu.Unlock()
}

// persist snapshot
func (rf *Raft) persistStatesAndSnapshot(rawSnapshot []byte) {
	w := bytes.NewBuffer(nil)
	e := labgob.NewEncoder(w)
	e.Encode(rf.curTerm)
	e.Encode(rf.votedFor)
	e.Encode(rf.lastIncludedIndex)
	e.Encode(rf.lastIncludedTerm)
	e.Encode(rf.logs)
	data := w.Bytes()
	rf.persister.SaveStateAndSnapshot(data, rawSnapshot)
}
