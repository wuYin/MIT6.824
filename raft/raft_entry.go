package raft

type LogEntry struct {
	Term    int
	Command interface{}
}

type AppendEntriesArgs struct {
	Term         int        // leader term
	LeaderID     int        // so follower can redirect clients
	PrevLogIndex int        // index of log entry immediately preceding new ones
	PrevLogTerm  int        // term of prevLogIndex entry
	Entries      []LogEntry // log entries to store (empty for heartbeat;may send more than one for efficiency)
	LeaderCommit int        // leader’s commitIndex
}

type AppendEntriesReply struct {
	Term          int  // currentTerm, for leader to update itself
	Succ          bool // true if follower contained entry matching prevLogIndex and prevLogTerm
	ConflictTerm  int  // if prevLogIndex not match, conflictTerm = logs[prevLogIndex].Term
	ConflictIndex int  // search log for the first index whose entry term equal to conflictTerm
}

func (rf *Raft) AppendEntries(args *AppendEntriesArgs, reply *AppendEntriesReply) {
	rf.mu.Lock()
	defer rf.mu.Unlock()
	reply.Term = rf.curTerm
	reply.Succ = false

	// 1. reply false if term < currentTerm (§5.1)
	if args.Term < rf.curTerm {
		return // leader expired
	}

	if args.Term > rf.curTerm {
		reply.Term = args.Term
		rf.back2Follower(args.Term, VOTE_NIL)
	}
	rf.resetElectTimer() // receiver RPC from current leader, reset timer

	// 2. Reply false if log doesn't contain an entry at prevLogIndex whose term matches prevLogTerm (§5.3)
	last := rf.lastIdx()
	if last < args.PrevLogIndex { // missing logs
		reply.ConflictTerm = NIL_TERM
		reply.ConflictIndex = last + 1
		return
	}

	// consistency check
	prevIdx := args.PrevLogIndex
	prevTerm := 0
	if prevIdx >= rf.lastIncludedIndex { //
		// pr("AppendEntries|Leader:%d -> Server:%d|%+v", args.LeaderID, rf.me, args)
		prevTerm = rf.getLog(prevIdx).Term
	}
	if prevTerm != args.PrevLogTerm { // term not match
		reply.ConflictTerm = prevTerm
		for i, e := range rf.logs {
			if e.Term == prevTerm {
				reply.ConflictIndex = i
				break
			}
		}
		return
	}
	// now peer and leader have same prevIndex and same prevTerm

	for i, e := range args.Entries {
		j := prevIdx + (1 + i)
		if j <= last {
			if rf.getLog(j).Term == e.Term {
				continue
			}
			// 3. if an existing entry conflicts with a new one (same index but different terms)
			// delete the existing entry and all that follow it
			rf.logs = rf.logs[:rf.subIdx(j)]
		}
		// 4. append any new entries not already in the log
		rf.logs = append(rf.logs, args.Entries[i:]...)
		rf.persistStates()
		break
	}
	// 5. if leaderCommit > commitIndex, set commitIndex = min(leaderCommit, index of last new entry)
	if args.LeaderCommit > rf.commitIndex {
		rf.commitIndex = min(args.LeaderCommit, rf.lastIdx()) // need to commit
		rf.checkApply()
	}
	rf.back2Follower(args.Term, args.LeaderID)

	reply.Succ = true
}

func (rf *Raft) sendAppendEntries(server int, args *AppendEntriesArgs, reply *AppendEntriesReply) bool {
	ok := rf.peers[server].Call("Raft.AppendEntries", args, reply)
	return ok
}
