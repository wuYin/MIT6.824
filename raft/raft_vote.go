package raft

const (
	VOTE_NIL = -1
)

// 2A 2B
type RequestVoteArgs struct {
	Term         int
	CandidateID  int
	LastLogIndex int // index of candidate's last log entry
	LastLogTerm  int // term  of candidate's last log entry
}

// 2A
type RequestVoteReply struct {
	Term        int
	VoteGranted bool
}

// 2A 2B
func (rf *Raft) RequestVote(args *RequestVoteArgs, reply *RequestVoteReply) {
	rf.mu.Lock()
	defer rf.mu.Unlock()
	reply.Term = rf.curTerm
	reply.VoteGranted = false

	// 1. Reply false if term < currentTerm (§5.1)
	if args.Term < rf.curTerm {
		return
	}
	if args.Term > rf.curTerm {
		reply.Term = args.Term
		rf.back2Follower(args.Term, VOTE_NIL)
	}
	// now terms are same

	// sorry, current term voted already
	if rf.votedFor != VOTE_NIL && rf.votedFor != args.CandidateID {
		return
	}

	// 2. If votedFor is null or candidateId, and candidate’s log is
	// at least as up-to-date as receiver’s log, grant vote (§5.2, §5.4)
	// if the logs have last entries with different terms, then the log with the later term is more up-to-date.
	// if the logs end with the same term, then whichever log is longer is more up-to-date.
	lastIndex, lastTerm := rf.lastIdx(), rf.lastTerm()
	if lastTerm > args.LastLogTerm { // term expired
		return
	}
	if lastTerm == args.LastLogTerm && lastIndex > args.LastLogIndex { // log missing
		return
	}
	// now last index and term both matched

	reply.VoteGranted = true
	rf.back2Follower(args.Term, args.CandidateID)
	rf.resetElectTimer() // grant a vote, reset timer

	// pr("Vote|%s:%d, Term:%d, Voted:%d, OK:%v --> Candidate:%d, Term:%d", rf.state, rf.me, rf.curTerm, rf.votedFor, reply.VoteGranted, args.CandidateID, args.Term)
	return
}

func (rf *Raft) sendRequestVote(server int, args *RequestVoteArgs, reply *RequestVoteReply) bool {
	ok := rf.peers[server].Call("Raft.RequestVote", args, reply)
	return ok
}
