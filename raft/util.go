package raft

import (
	"fmt"
	"log"
	"sort"
	"strings"
	"time"
)

// Debugging
const debug = false

type LogWriter struct{}

func (w LogWriter) Write(bytes []byte) (int, error) {
	return fmt.Print(time.Now().Format("05.999"), "\t", string(bytes))
}

func pr(format string, vs ...interface{}) {
	log.SetFlags(0)
	log.SetOutput(new(LogWriter))
	if !strings.HasSuffix(format, "\n") {
		format += "\n"
	}
	if debug {
		log.Printf(format, vs...)
	}
}

func pf(format string, vs ...interface{}) {
	if debug {
		fmt.Printf(format, vs...)
	}
}

func since(start time.Time) string {
	n := time.Since(start).Nanoseconds() / int64(time.Millisecond)
	return fmt.Sprintf("%dms", n)
}

func min(x int, nums ...int) int {
	for _, num := range nums {
		if x > num {
			x = num
		}
	}
	return x
}

func max(x int, nums ...int) int {
	for _, num := range nums {
		if x < num {
			x = num
		}
	}
	return x
}

func revSort(nums []int) {
	sort.Ints(nums)
	for i, j := 0, len(nums)-1; i <= j; i, j = i+1, j-1 {
		nums[i], nums[j] = nums[j], nums[i]
	}
}
