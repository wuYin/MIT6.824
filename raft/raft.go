package raft

import (
	"MIT6.824/labrpc"
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

type ApplyMsg struct {
	CommandValid bool
	Command      interface{}
	CommandIndex int
}

type Raft struct {
	mu        sync.RWMutex        // Lock to protect shared access to this peer's state
	peers     []*labrpc.ClientEnd // RPC end points of all peers
	persister *Persister          // Object to hold this peer's persisted state
	me        int                 // this peer's index into peers[]

	// persistent states
	curTerm  int        // latest term server has seen(initialized to 0 on first boot, increases monotonically)
	votedFor int        // candidateId that received vote in current term(or null if none)
	logs     []LogEntry // log entries; each entry contains command for state machine, and term when entry was received by leader(first index is 1)

	// volatile states
	commitIndex int // index of highest log entry known to be committed(initialized to 0, increases monotonically)
	lastApplied int // index of highest log entry applied to state machine(initialized to 0, increases monotonically)

	// leader states
	nextIndex  []int // for each server, index of the next log entry to send to that server(initialized to leader last log index + 1)
	matchIndex []int // for each server, index of highest log entry known to be replicated on server(initialized to 0, increases monotonically)

	// snapshot
	lastIncludedIndex int // the snapshot replaces all entries up through and including this index
	lastIncludedTerm  int // term of lastIncludedIndex

	// implementation
	state     PeerState
	timer     *RaftTimer
	applyCh   chan ApplyMsg
	crashed   bool
	syncConds []*sync.Cond // every Raft peer has a condition, use for trigger AppendEntries RPC
}

func (rf *Raft) String() string {
	// just use for go test -race
	// rf.mu.RLock()
	// defer rf.mu.RUnlock()

	s := fmt.Sprintf("%s:%d, Term:%d, Voted:%d, Timer:%s, State:%s, Logs:%v",
		rf.state, rf.me, rf.curTerm, rf.votedFor, rf.timer, rf.state, rf.logs)

	if rf.state == Leader {
		s += fmt.Sprintf(", Next:%v, Match:%v", rf.nextIndex, rf.matchIndex)
	}
	if rf.crashed {
		s += "[Crashed]" + s
	}
	return s
}

func (rf *Raft) GetState() (int, bool) {
	rf.mu.RLock()
	defer rf.mu.RUnlock()
	return rf.curTerm, rf.state == Leader
}

func (rf *Raft) Kill() {
	rf.mu.Lock()
	rf.crashed = true
	rf.mu.Unlock()
}

func Make(peers []*labrpc.ClientEnd, me int, persister *Persister, applyCh chan ApplyMsg) *Raft {
	rf := &Raft{}
	rf.mu = sync.RWMutex{}
	rf.peers = peers
	rf.persister = persister
	rf.me = me

	rf.curTerm = 0
	rf.votedFor = VOTE_NIL
	rf.logs = []LogEntry{{Term: 0, Command: nil}} // first index is 1

	// rf.commitIndex = 0
	// rf.lastApplied = 0

	rf.matchIndex = make([]int, len(rf.peers))

	rf.state = Follower
	rf.initElectTimer()
	rf.applyCh = applyCh
	rf.crashed = false
	rf.syncConds = make([]*sync.Cond, len(rf.peers))
	for i := range rf.peers {
		rf.syncConds[i] = sync.NewCond(&rf.mu)
	}

	// initialize from state persisted before a crash
	rf.readPersist(persister.ReadRaftState())
	rf.nextIndex = make([]int, len(rf.peers))
	for i := range rf.peers {
		rf.nextIndex[i] = rf.lastIdx() + 1 // initialized to leader last log index + 1
	}
	rf.commitIndex = rf.lastIncludedIndex
	rf.lastApplied = rf.lastIncludedIndex

	go rf.waitVote() // wait for election timer timeout and start vote

	// pr("Make|%v", rf)
	return rf
}

// start vote
// leader can start vote repeatedly, such as 2 nodes are crashed in 3 nodes cluster
// leader should reset election timeout when heartbeat to prevent this
func (rf *Raft) vote() {
	// pr("Timeout|%v", rf)
	rf.mu.Lock()
	rf.curTerm++
	rf.state = Candidate
	rf.votedFor = rf.me
	rf.resetElectTimer() // vote to myself, reset timer
	rf.persistStates()

	i := rf.lastIdx()
	args := RequestVoteArgs{
		Term:         rf.curTerm,
		CandidateID:  rf.me,
		LastLogIndex: i,
		LastLogTerm:  rf.getLog(i).Term,
	}
	rf.mu.Unlock()

	replyCh := make(chan RequestVoteReply, len(rf.peers))
	var wg sync.WaitGroup
	for i := range rf.peers {
		if i == rf.me {
			continue
		}

		wg.Add(1)
		go func(server int) {
			defer wg.Done()
			var reply RequestVoteReply
			respCh := make(chan struct{})
			go func() {
				if rf.sendRequestVote(server, &args, &reply) {
					respCh <- struct{}{}
				}
			}()
			select {
			// this should last too long, the partition leader should back to follower in reasonable time, such as 200ms
			case <-time.After(RPC_CALL_TIMEOUT):
				return
			case <-respCh:
				replyCh <- reply
			}
		}(i)
	}
	go func() {
		wg.Wait()
		close(replyCh) // avoid goroutine leak
	}()

	votes := int32(1)
	majority := int32(len(rf.peers)/2 + 1)
	for reply := range replyCh {
		rf.mu.Lock()
		if reply.Term > rf.curTerm { // higher term leader
			rf.back2Follower(reply.Term, VOTE_NIL)
			rf.mu.Unlock()
			return
		}
		if rf.state != Candidate || reply.Term < rf.curTerm { // current term changed already
			rf.mu.Unlock()
			return
		}

		if reply.VoteGranted {
			atomic.AddInt32(&votes, 1)
		}
		if atomic.LoadInt32(&votes) >= majority { // if reach majority earlier, shouldn't wait crashed peer for timeout
			rf.state = Leader
			rf.mu.Unlock()
			go rf.sync()
			go rf.heartbeat()

			// pr("Vote|Win|%v", rf)
			return
		}
		rf.mu.Unlock()
	}

	// split vote
	// pr("Vote|Split|%s:%d, Term:%d, Vote:%d < %d", rf.state, rf.me, rf.curTerm, votes, majority)
	rf.mu.Lock()
	rf.back2Follower(STAY_TERM, VOTE_NIL)
	rf.mu.Unlock()
}

// wait for start next round vote
func (rf *Raft) waitVote() {
	for {
		select {
		case <-rf.timer.t.C: // election timeout
			rf.vote()
		}
	}
}

// apply log in (lastApplied, commitIndex]
func (rf *Raft) checkApply() {
	for rf.lastApplied < rf.commitIndex {
		rf.lastApplied++
		rf.applyCh <- ApplyMsg{
			CommandValid: true,
			Command:      rf.getLog(rf.lastApplied).Command,
			CommandIndex: rf.lastApplied,
		}
		// pr("Apply|Succ|%s:%d, Msg:%v", rf.state, rf.me, msg)
	}
}
