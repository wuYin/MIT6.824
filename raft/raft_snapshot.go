package raft

type InstallSnapshotArgs struct {
	Term              int    // leader's term
	LeaderId          int    // so follower can redirect clients
	LastIncludedIndex int    // the snapshot replaces all entries up through and including this index
	LastIncludedTerm  int    // term of lastIncludedIndex
	Data              []byte // raw bytes of the snapshot chunk, starting at offset
	// Offset            int    // byte offset where chunk is positioned in the snapshot file
	// Done              bool   // true if this is the last chunk
}

type InstallSnapshotReply struct {
	Term int // currentTerm, for leader to update itself
}

func (rf *Raft) InstallSnapshot(args *InstallSnapshotArgs, reply *InstallSnapshotReply) {
	rf.mu.Lock()
	defer rf.mu.Unlock()
	reply.Term = rf.curTerm

	// 1. reply false if term < currentTerm
	if args.Term < rf.curTerm {
		return
	}
	if args.Term > rf.curTerm {
		reply.Term = args.Term
		rf.back2Follower(args.Term, VOTE_NIL)
	}
	rf.resetElectTimer()

	// check snapshot may expired by lock competition, otherwise rf.logs may overflow below
	if args.LastIncludedIndex <= rf.lastIncludedIndex {
		return
	}

	// 2. Create new snapshot file if first chunk (offset is 0)
	// 3. Write data into snapshot file at given offset
	// 4. Reply and wait for more data chunks if done is false
	// 5. Save snapshot file, discard any existing or partial snapshot with a smaller index

	// 6. If existing log entry has same index and term as snapshot's last included entry, retain log entries following it and reply
	if args.LastIncludedIndex < rf.lastIdx() {
		// the args.LastIncludedIndex log has agreed, if there are more logs, just retain them
		rf.logs = rf.logs[args.LastIncludedIndex-rf.lastIncludedIndex:]
	} else {
		// 7. Discard the entire log
		// empty log use for AppendEntries RPC consistency check
		rf.logs = []LogEntry{{Term: args.LastIncludedTerm, Command: nil}}
	}

	// update snapshot state and persist them
	rf.lastIncludedIndex = args.LastIncludedIndex
	rf.lastIncludedTerm = args.LastIncludedTerm
	rf.persistStatesAndSnapshot(args.Data)

	// force the follower's log catch up with leader
	rf.commitIndex = max(rf.commitIndex, args.LastIncludedIndex)
	rf.lastApplied = max(rf.lastApplied, args.LastIncludedIndex)

	// 8. Reset state machine using snapshot contents (and load snapshot's cluster configuration)
	rf.applyCh <- ApplyMsg{
		CommandValid: false, // it's snapshot raw data, not a command
		CommandIndex: -1,
		Command:      args.Data,
	}
}

func (rf *Raft) sendInstallSnapshot(server int, args *InstallSnapshotArgs, reply *InstallSnapshotReply) bool {
	ok := rf.peers[server].Call("Raft.InstallSnapshot", args, reply)
	return ok
}
