package mapreduce

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"sort"
)

func doReduce(
	jobName string, // the name of the whole MapReduce job
	reduceTask int, // which reduce task this is
	outFile string, // write the output here
	nMap int,       // the number of map tasks that were run ("M" in the paper)
	reduceF func(key string, values []string) string,
) {
	// 读取 map task 处理后的输出结果，调用用户自定义的 reduce func
	// fmt.Printf("doReduce: reduceTask:%d\t nMap:%d\t outFile:%s\n", reduceTask, nMap, outFile)

	// 读取所有分区中间文件中的所有 key-value pair
	kvMap := make(map[string][]string) // (key, list(value))
	for i := 0; i < nMap; i++ {
		fName := reduceName(jobName, i, reduceTask)
		data, err := ioutil.ReadFile(fName)
		if err != nil {
			log.Fatal("common reduce: ", err)
		}

		var kvs []KeyValue
		if err = json.Unmarshal(data, &kvs); err != nil {
			log.Fatal("common reduce: ", err)
		}

		for _, kv := range kvs {
			kvMap[kv.Key] = append(kvMap[kv.Key], kv.Value)
		}
	}

	// reduce 任务并处理 key 排序
	ks := make([]string, 0, len(kvMap))
	resMap := make(map[string]string, len(kvMap))
	for k, vs := range kvMap {
		ks = append(ks, k)
		resMap[k] = reduceF(k, vs)
	}
	sort.Strings(ks)

	// 将 reduce 聚合及结果写入最终文件
	outF, err := os.OpenFile(outFile, os.O_WRONLY|os.O_CREATE, 0655)
	if err != nil {
		log.Fatal("common reduce: ", err)
	}
	defer outF.Close()
	enc := json.NewEncoder(outF)
	for _, k := range ks {
		enc.Encode(KeyValue{Key: k, Value: resMap[k]})
	}

	//
	// doReduce manages one reduce task: it should read the intermediate
	// files for the task, sort the intermediate key/value pairs by key,
	// call the user-defined reduce function (reduceF) for each key, and
	// write reduceF's output to disk.
	//
	// You'll need to read one intermediate file from each map task;
	// reduceName(jobName, m, reduceTask) yields the file
	// name from map task m.
	//
	// Your doMap() encoded the key/value pairs in the intermediate
	// files, so you will need to decode them. If you used JSON, you can
	// read and decode by creating a decoder and repeatedly calling
	// .Decode(&kv) on it until it returns an error.
	//
	// You may find the first example in the golang sort package
	// documentation useful.
	//
	// reduceF() is the application's reduce function. You should
	// call it once per distinct key, with a slice of all the values
	// for that key. reduceF() returns the reduced value for that key.
	//
	// You should write the reduce output as JSON encoded KeyValue
	// objects to the file named outFile. We require you to use JSON
	// because that is what the merger than combines the output
	// from all the reduce tasks expects. There is nothing special about
	// JSON -- it is just the marshalling format we chose to use. Your
	// output code will look something like this:
	//
	// enc := json.NewEncoder(file)
	// for key := ... {
	// 	enc.Encode(KeyValue{key, reduceF(...)})
	// }
	// file.Close()
	//
	// Your code here (Part I).
	//
}
