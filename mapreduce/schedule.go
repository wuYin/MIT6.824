package mapreduce

import (
	"fmt"
	"sync"
)

//
// schedule() starts and waits for all tasks in the given phase (mapPhase
// or reducePhase). the mapFiles argument holds the names of the files that
// are the inputs to the map phase, one per map task. nReduce is the
// number of reduce tasks. the registerChan argument yields a stream
// of registered workers; each item is the worker's RPC address,
// suitable for passing to call(). registerChan will yield all
// existing registered workers (if any) and new ones as they register.
//
// schedule 将 map/reduce tasks 的执行并发化
func schedule(jobName string, mapFiles []string, nReduce int, phase jobPhase, registerChan chan string) {
	var ntasks int
	var n_other int // number of inputs (for reduce) or outputs (for map)
	switch phase {
	case mapPhase:
		ntasks = len(mapFiles) // 输入文件数量
		n_other = nReduce      // 中间文件数量
	case reducePhase:
		ntasks = nReduce        // reduce worker 数量
		n_other = len(mapFiles) // 输入文件数量
	}

	fmt.Printf("Schedule: %v %v tasks (%d I/Os)\n", ntasks, phase, n_other)

	// All ntasks tasks have to be scheduled on workers. Once all tasks
	// have completed successfully, schedule() should return.
	//
	// Your code here (Part III, Part IV).
	//

	var wg sync.WaitGroup
	workerCh := make(chan string, 100) // map 任务完成了不缓存 2 个 worker

	// map worker 处理输入文件或 reduce worker 聚合数据
	for i := 0; i < ntasks; i++ {
		wg.Add(1)
		go func(n int) {
			defer wg.Done()

			worker := blockSelectWorker(registerChan, workerCh) // worker 选中了立刻上锁
			locks[worker].Lock()

			args := DoTaskArgs{
				JobName:       jobName,
				File:          mapFiles[n], // 需处理的输入文件
				Phase:         phase,
				TaskNumber:    n,
				NumOtherPhase: n_other,
			}

			succ := call(worker, "Worker.DoTask", args, nil)
			for !succ {
				fmt.Printf("[FAIL]: schedule: %s: task %d failed by %s\n", args.Phase, args.TaskNumber, worker)
				if _, ok := locks[worker]; ok {
					locks[worker].Unlock() // 释放旧锁并删除 worker
					l.Lock()
					delete(locks, worker)
					l.Unlock()
				}

				// worker 节点失效直接将其 task 分配给其他 worker 处理直到成功为止
				worker = blockSelectWorker(registerChan, workerCh)
				locks[worker].Lock()                            // 上新锁
				succ = call(worker, "Worker.DoTask", args, nil) //
				if !succ {
					locks[worker].Unlock()
				}
			}

			workerCh <- worker
			locks[worker].Unlock()
		}(i)
	}

	wg.Wait()
	fmt.Printf("Schedule: %v done\n", phase)
}

var (
	locks = make(map[string]*sync.Mutex)
	l     sync.Mutex
)

// 阻塞选取可用的 worker
func blockSelectWorker(registerChan, workerCh chan string) (worker string) {
	// 这里改一下机制，旧的 worker 用完了直接传过来
	// channel 本身就是原子性的，不要一把锁到处用，免得隐藏有竞态条件
	for {
		select {
		case worker = <-registerChan:
			l.Lock()
			locks[worker] = &sync.Mutex{}
			l.Unlock()
			return
		case worker = <-workerCh:
			return
		}
	}
}
