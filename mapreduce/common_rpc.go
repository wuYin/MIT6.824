package mapreduce

import (
	"fmt"
	"net/rpc"
)

// What follows are RPC types and methods.
// Field names must start with capital letters, otherwise RPC will break.

// DoTaskArgs holds the arguments that are passed to a worker when a job is
// scheduled on it.
// 每个 task 执行所必需的参数
type DoTaskArgs struct {
	JobName    string
	File       string   // only for map, the input file
	Phase      jobPhase // are we in mapPhase or reducePhase?
	TaskNumber int      // this task's index in the current phase

	// NumOtherPhase is the total number of tasks in other phase; mappers
	// need this to compute the number of output bins, and reducers needs
	// this to know how many input files to collect.
	NumOtherPhase int
}

// ShutdownReply is the response to a WorkerShutdown.
// It holds the number of tasks this worker has processed since it was started.
// WorkerShutdown 方法的返回值，记录当前 worker 从始至终一共处理了多少个 task
type ShutdownReply struct {
	Ntasks int
}

// RegisterArgs is the argument passed when a worker registers with the master.
// 注册 worker 只需 RPC 地址
type RegisterArgs struct {
	Worker string // the worker's UNIX-domain socket name, i.e. its RPC address
}

// call() sends an RPC to the rpcname handler on server srv
// with arguments args, waits for the reply, and leaves the
// reply in reply. the reply argument should be the address
// of a reply structure.
// 返回值参数一般是定义好的结构，直接 assert 取值
//
// call() returns true if the server responded, and false if call()
// received no reply from the server. reply's contents are valid if
// and only if call() returned true.
//
// you should assume that call() will time out and return
// false after a while if it doesn't get a reply from the server.
//
// please use call() to send all RPCs. please don't change this
// function.
//
func call(srv string, rpcname string, args interface{}, reply interface{}) bool {
	c, errx := rpc.Dial("unix", srv)
	if errx != nil {
		return false
	}
	defer c.Close()

	err := c.Call(rpcname, args, reply) // 执行 RPC 调用并阻塞等待返回或调用超时
	if err == nil {
		return true
	}

	fmt.Println(err)
	return false
}
